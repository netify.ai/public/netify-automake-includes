#!/bin/sh

set -e

while [ $# -gt 0 ]; do
	if [ -d "/usr/include/$1/$1" ]; then
		mv -v "/usr/include/$1/$1" /tmp
		rmdir -v "/usr/include/$1"
		mv -v "/tmp/$1" "/usr/include/"
	fi
	shift
done

exit $?
